package org.example;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import org.apache.logging.log4j.*; 

public class Cliente {
    private static Logger logger = LogManager.getLogger("loggercito");
    //Constructor
    private Cliente(){}

    public static void main( String[] args )
    {
        String direccionServidor;
      
            Scanner lecturaTeclado = new Scanner(System.in);
            System.out.print("Introduzca la dirección IP o DNS del servidor RMI: ");
            direccionServidor = lecturaTeclado.nextLine();
        //Si no se indica dirección de servidor, se asume "null" (localhost)
        if (direccionServidor.equals("")) {
            direccionServidor = null;
        }
        logger.info("Preparando servidor rmiregistry...");
        try {
            //Obteniendo registro de rmiregistry
            System.out.println("Obteniendo registro de rmiregistry...");
            logger.info("Obteniendo registro de rmiregistry...");
            Registry registry = LocateRegistry.getRegistry(direccionServidor);

            //Buscando el objeto RMI remoto
            System.out.println("Buscando objeto RMI rmiClima y creando stub...");
            logger.info("Buscando objeto RMI rmiClima y creando stub...");

            ClimaRmi stub = (ClimaRmi) registry.lookup("rmiClima");
            System.out.println("Objeto rmiClima remoto encontrado...");
            System.out.println();
            System.out.println("Ejecutando método remoto RMI para obtener info del clima en el servidor...");
            logger.info("objeto rmiClima enocntrado");
            String infoRecibida = stub.infoClimaRmi();
            System.out.println(infoRecibida);
            System.out.println();
            System.out.println("Fin programa cliente RMI");
            logger.info("EL programa cliente rmi finalizo correctamente");
        } catch (Exception e) {
            System.out.println("Error en cliente RMI: " + e.getMessage());
        }
    }

}