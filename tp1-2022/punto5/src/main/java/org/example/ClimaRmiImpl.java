package org.example;

//Implementa la interfaz remota
public class ClimaRmiImpl implements ClimaRmi {
    int temperatura = (int) Math.floor(Math.random()    *(30-0+1)+0);
    int probPrecipitaciones = (int) Math.floor(Math.random()*(100-0+1)+0);
    static int humedad = (int) Math.floor(Math.random()*(100-0+1)+0);
    int viento = (int) Math.floor(Math.random()*(10-0+1)+0);

    // Devuelve info del clima
    @Override
    public String infoClimaRmi() {
       return "la temperatura actual es de "+temperatura
       +", con probabilidades de precicpitaciones de un "+probPrecipitaciones
       +", con una humedad de un "+humedad
       +", con un viento de " + viento+" kmph";
    }
}