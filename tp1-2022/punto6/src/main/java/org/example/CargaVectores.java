package org.example;

import java.util.Arrays;
import java.util.Scanner;

public class CargaVectores {

    Scanner leer = new Scanner(System.in);
    
    public int[] cargarVector(int lengthVector){
        //crea el primer vector con valor parametrizado
        int vec1[] = new int[lengthVector];
        //recorre el vector para cargarlo
        for(int i=0; i<vec1.length; i++){
           System.out.print("\nIngresar un numero entero para la posicion"+" " +i+":");
           vec1[i]=leer.nextInt();
        }
        return vec1;
    }

    public String mostrarVectores(int v1[], int v2[], int vResultado[]){
        return "\r\n Vector 1" + Arrays.toString(v1) + "\r\n Vector 2" + Arrays.toString(v2) + "\r\n Vector Resultado" + Arrays.toString(vResultado) ;
    }

    public String mostrarVectores(int[] v1, int[] v2) {
        return "\r\n Vector 1" + Arrays.toString(v1) + "\r\n Vector 2" + Arrays.toString(v2);
    }
}
