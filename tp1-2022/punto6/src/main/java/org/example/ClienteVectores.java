package org.example;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import org.apache.logging.log4j.*; 

public class ClienteVectores {
    private static Logger logger = LogManager.getLogger("loggercito");
    //Constructor
    private ClienteVectores(){}
   public static void main( String[] args )
    {
        String direccionServidor;
        boolean mostrarMenu=true;
        int[] v1;
        int[] v2;
        int[] vResultado;
        int longitudVectores=4;
        CargaVectores cargaVectores = new CargaVectores();
        Scanner leer = new Scanner(System.in);

            Scanner lecturaTeclado = new Scanner(System.in);
            System.out.print("Introduzca la dirección IP o DNS del servidor RMI: ");
            direccionServidor = lecturaTeclado.nextLine();
        //Si no se indica dirección de servidor, se asume "null" (localhost)
        if (direccionServidor.equals("")) {
            direccionServidor = null;
        }
        logger.info("IP del servidor ingresada: "+direccionServidor);
        try {
            //Obteniendo registro de rmiregistry
            System.out.println("Obteniendo registro de rmiregistry...");
            logger.info("Obteniendo registro de rmiregistry...");

            Registry registry = LocateRegistry.getRegistry(direccionServidor);

            //Buscando el objeto RMI remoto
            System.out.println("Buscando objeto RMI rmiVectores y creando stub...");
            VectoresRmi stub = (VectoresRmi) registry.lookup("rmiVectores");
            System.out.println("Objeto rmiVectores remoto encontrado...");
            System.out.println();
            logger.info("Objeto rmiVectores remoto encontrado...");
            System.out.print("Ingrese tamaño de los vectores a trabajar:");
            longitudVectores = leer.nextInt();
            logger.info("tamanio de vectores ingresado: "+longitudVectores);
            System.out.println("Preparando carga del Vector 1...");
            v1= cargaVectores.cargarVector(longitudVectores);
            System.out.println("Preparando carga del Vector 2...");
            v2= cargaVectores.cargarVector(longitudVectores);
            while(mostrarMenu){
                System.out.println("\r\nIngresar una opcion");
                System.out.println("1. sumar vectores \r\n");
                System.out.println("2. restar vectores\r\n");
                System.out.println("3. sumar vectores modificandolos* \r\n");
                System.out.println("4. restar vectores modificandolos* \r\n");
                System.out.println("* La modificacion consiste en poner en cero los valores de v1 y v2 enviados como parametro al servidor rmi \r\n");
                System.out.println("5. Volver a cargar\r\n");
                System.out.println("6. Mostrar vectores \r\n");
                System.out.println("7. Salir\r\n");
                String str = leer.nextLine();
                logger.info("opcion ingresada: "+str);
                switch (str) {
                    case "1":
                        System.out.println("\r\nSumando Vectores");
                        vResultado = stub.sumarVectores(v1, v2);
                        System.out.println("\r\n Resultado de la operacion: ");
                        System.out.println(cargaVectores.mostrarVectores(v1, v2, vResultado));
                        System.out.println("\r\n pulsar enter para continuar");
                        str = leer.nextLine();
                        break;
                    case "2":
                        System.out.println("\r\n restando Vectores");
                        vResultado = stub.restarVectores(v1, v2);
                        System.out.println("\r\n Resultado de la operacion: ");
                        System.out.println(cargaVectores.mostrarVectores(v1, v2, vResultado));
                        System.out.println("\r\n pulsar enter para continuar");
                        str = leer.nextLine();
                        break;
                    case "3":
                        System.out.println("\r\nSumando Vectores");
                        vResultado = stub.sumarVectoresModificando(v1, v2);
                        System.out.println("\r\n Resultado de la operacion: ");
                        System.out.println(cargaVectores.mostrarVectores(v1, v2, vResultado));
                        System.out.println("\r\n pulsar enter para continuar");
                        str = leer.nextLine();
                        break;
                    case "4":
                    System.out.println("\r\n restando Vectores");
                    vResultado = stub.restarVectoresModificando(v1, v2);
                    System.out.println("\r\n Resultado de la operacion: ");
                    System.out.println(cargaVectores.mostrarVectores(v1, v2, vResultado));
                    System.out.println("\r\n pulsar enter para continuar");
                    str = leer.nextLine();
                        break;
                    case "5":
                        System.out.print("Ingrese tamaño de los vectores a trabajar:");
                        longitudVectores = leer.nextInt();
                        System.out.println("Preparando carga del Vector 1...");
                        v1= cargaVectores.cargarVector(longitudVectores);
                        System.out.println("Preparando carga del Vector 2...");
                        v2= cargaVectores.cargarVector(longitudVectores);                        break;
                    case "6":
                        System.out.println(cargaVectores.mostrarVectores(v1, v2));
                        System.out.println("\r\n pulsar enter para continuar");
                        str = leer.nextLine();
                        break;
                        case "7":
                        mostrarMenu = false;
                        break;
                    default:
                        break;
                } 
            }
            System.out.println("Fin programa cliente RMI");
        } catch (Exception e) {
            logger.error("exeption: "+e.getMessage());
            e.printStackTrace();        }
    }
 
}