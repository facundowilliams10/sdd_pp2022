package org.example;


import java.net.ServerSocket;
import java.net.Socket;
import org.example.Message;
import java.util.ArrayList;
import java.util.List;
//import java.util.logging.Logger;

import org.apache.logging.log4j.*;

/**
 * Hello world!
 *
 */
public class ServerTcpPunto3
{
    private static Logger log = LogManager.getLogger(ServerTcpPunto3.class.getName());
    public ServerTcpPunto3(int port){
        /*
        Capa de Trasnporte: TCP / UDP
         */
        try{
            ServerSocket ss = new ServerSocket(port);
            log.info("Server has been started on port:  "+port);
            List<Message> mensajes = new ArrayList<>();
            List<String> users = new ArrayList<String>();
        
            while (true){
                Socket client = ss.accept();

                //log.info ("Atendiendo al cliente: "+client.getPort());

                // 3 pasos
                // 1er paso
                ServerHilo sh = new ServerHilo(client, mensajes, users);
                // 2do paso
                Thread serverThread = new Thread(sh);
                // 3er paso
                serverThread.start();

            }
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
        }


    }
    public static void main( String[] args )
    {
        // parametros de consola
        int port = 9090;
        ServerTcpPunto3 server = new ServerTcpPunto3(port);
        
    }
}
