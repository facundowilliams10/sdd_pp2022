#!/bin/bash
#[STEP 0] - crear ejecutable JAR
mvn clean;
mvn package;
#[STEP 1] - construir imagen (en base al Dockerfile)
docker build -t williamsoft/punto1y2:latest .
#[STEP 2] - subir a dockerHUB la imagen creada
docker push williamsoft/punto1y2:latest
#[STEP 3] - parar y eliminar la versión anterior del contenedor
#docker stop punto1y2
#docker rm punto1y2
#[STEP 4] - poner a correr la nueva versión del contenedor
docker run --name punto1y2 -d -p 9090:9090 punto1y2:latest
