package org.example;


import java.net.ServerSocket;
import java.net.Socket;
import org.apache.logging.log4j.*; 


public class ServerTcp
{
    //private final Logger logger = LoggerFactory.getLogger(ServerTcp.class);
    private static Logger logger = LogManager.getLogger(ServerTcp.class.getName());
    public ServerTcp(int port){
        /*
        Capa de Trasnporte: TCP / UDP
         */
        try{
            ServerSocket ss = new ServerSocket(port);
            
            logger.info("Se abre un socket tipo servidor que aguarda peticiones en el puerto "+port);
            
            while (true){
                Socket client = ss.accept();
                /*
                Particular de java como maneja la E/S del socket ->
                                                    Canal de Entrada -> server (lee)
                                                    Canal de Salida -> server  (escribe)
                --> Canal
                    * String <-- JSON <-- TEXT
                    * Buffer
                    * Object Serializable (JAVA) --> Public class Auto implements Serializable {}
                 */
                //System.out.println("Atendiendo al cliente: "+client.getPort());
                logger.trace("Atendiendo al cliente: "+client.getPort());
                // 3 pasos
                // 1er paso
                ServerHilo sh = new ServerHilo(client);
                // 2do paso
                Thread serverThread = new Thread(sh);
                // 3er paso
                serverThread.start();

            }
        }catch (Exception e){
            logger.error("msg: "+e.getMessage());
            e.printStackTrace();
        }


    }
    public static void main( String[] args )
    {
        int threadId = (int) Thread.currentThread().getId();
        String logName = ServerTcp.class.getSimpleName()+"-"+threadId;
        System.setProperty("log.name", logName);
        System.setProperty("project.name", "punto1y2");
  
        logger.info("TP1 - Punto 1 y 2");
        
        // parametros de consola
        
        int port = 9090;
        ServerTcp server = new ServerTcp(port);
    }
}
