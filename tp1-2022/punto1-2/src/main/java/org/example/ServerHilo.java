package org.example;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerHilo implements Runnable {
    BufferedReader canalEntrada;
    PrintWriter canalSalida;
    Socket client;

    public ServerHilo(Socket client) {

        this.client = client;
    }

    @Override
    public void run() {
        // imaginando un proceso que lleva tiempo
        try {
            InputStream in = this.client.getInputStream(); 
            InputStreamReader inr = new InputStreamReader(in);
            BufferedReader canalEntrada = new BufferedReader(inr);

            PrintWriter canalSalida = new PrintWriter(this.client.getOutputStream(), true);
            Thread.sleep(2000);
            canalSalida.println("el_server_responde"); 

            boolean flag= true;
            while(flag){

                String str = canalEntrada.readLine();
                canalSalida.println("Reply: "+ str);

                if(str.equals("exit")){

                    client.close();
                    flag= false;

                }
                System.out.println(str);  

            }    
        }catch (Exception e){

        }


    }
}
