package org.example;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import org.apache.logging.log4j.*; 

public class Servidor extends EjecutorRmiImpl {
    private static Logger logger = LogManager.getLogger("loggercito");

    public static void main( String[] args )
    {
        try {
            //Facundo: Esta linea me volvio loco, si no se ejecuta el rmiregistry en el mismo path no funciona
            LocateRegistry.createRegistry(1099);
            System.out.println("Preparando servidor rmiregistry...");
            System.out.println("Preparando servidor RMI...");
            // Instancia la clase implementada
            EjecutorRmiImpl ejecutor = new EjecutorRmiImpl();
            // Exporta el objeto de la clase implementada
            // Con port=0 se usará el puerto por defecto de RMI, el 1099
            EjecutorRmi stub = (EjecutorRmi) UnicastRemoteObject.exportObject(ejecutor, 0);
            // Vinculamos el objeto remoto (stub) en el registro (rmiregistry)
            Registry registry = LocateRegistry.getRegistry();
            // Enlaza el stub y nombramos el objeto remoto RMI como "EjecutorRmi"
            registry.bind("EjecutorRmi", stub);
            System.out.println("Servidor RMI escuchando...");
            logger.info("el servidor esta escuchando");
        } catch (Exception e) {
            logger.error("exception: "+e.getMessage());
            e.printStackTrace();            }
    }
}