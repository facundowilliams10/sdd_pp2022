package org.example;

import java.io.Serializable;

//interfaz remota para la aplicación
public interface Tarea extends Serializable {

    public double ejecutar();
}