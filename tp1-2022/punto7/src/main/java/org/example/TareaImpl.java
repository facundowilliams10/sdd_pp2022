package org.example;

import java.io.Serializable;

//clase TareaImpl implementa la interfaz Tarea
public class TareaImpl implements Tarea {
    // Devuelve el valor PI
    @Override
    public double ejecutar(){
        double num = Math.PI;
        return num;
    }
}