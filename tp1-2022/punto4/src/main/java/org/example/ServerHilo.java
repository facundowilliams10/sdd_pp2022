package org.example;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerHilo implements Runnable {
    BufferedReader canalEntrada;
    PrintWriter canalSalida;
    Socket client;
    List<String> users;
    List<Message> mensajes;

    public ServerHilo(Socket client, List<Message> mensajes, List<String> users) {
        this.mensajes = mensajes;
        this.client = client;
        this.users = users;
    }

    private boolean existeUser(List<String> users, String user){

        int i = 0;
        boolean repetido = false;
        while (i < users.size() && !repetido) {
            if (users.get(i).equals(user)){
                repetido =  true;
            }
            i++;
        }
        return repetido;      
    }  
    private void menuPrincipal(String name){
        try {
                  
            canalSalida.println("\r\nIngrese la opcion que desee");
            canalSalida.println("1. Enviar mensaje \r\n");
            canalSalida.println("2. Leer mensajes\r\n");
            canalSalida.println("3. Salir\r\n");
            String str = canalEntrada.readLine();
            String destino;
            String body;
            Message mensaje;

            switch (str) {
                case "1":
                    canalSalida.println("\r\nIngrese el nombre de destino");
                    destino = canalEntrada.readLine();
                    canalSalida.println("\r\nEscriba el mensaje");
                    body = canalEntrada.readLine();
                    mensaje = new Message(name, destino, body);
                    this.mensajes.add(mensaje);
                    menuPrincipal(name);
                    break;
                case "2":
                    List<Message> mensajesEliminados = new ArrayList<>();
                    for (Message msj : this.mensajes) {
                        if (msj.getTo().equals(name)) {
                            mensajesEliminados = new ArrayList<>();
                            canalSalida.println("\r\nMensaje de: "+msj.getFrom());
                            canalSalida.println("\r\nMensaje: "+ msj.getBody());
                            canalSalida.println("----------------------------");
                            canalSalida.println("\r\n1. Eliminar");
                            canalSalida.println("\r\n0. Volver");
                            str = canalEntrada.readLine();
                            switch (str) {
                                case "1":
                                    mensajesEliminados.add(msj);
                                    break;
                                case "0":
                                    menuPrincipal(name);
                                    break; 
                                default:
                                    menuPrincipal(name);
                                    break;
                            }
                        }
                    }
                    mensajes.removeAll(mensajesEliminados);
                    menuPrincipal(name);
                    break;
                case "3":
                    client.close();
                    users.remove(name);
                    break;
                default:
                    break;
            } 
        } catch (Exception e) {
            //TODO: handle exception            
            //e.printStackTrace();
        }
    }

    @Override
    public void run() {
        // imaginando un proceso que lleva tiempo
        try {
            InputStream in = this.client.getInputStream(); 
            InputStreamReader inr = new InputStreamReader(in);
            canalEntrada = new BufferedReader(inr);

            canalSalida = new PrintWriter(this.client.getOutputStream(), true);
            Thread.sleep(2000);
            canalSalida.println("Ingrese su nombre\r\n"); 
            
            String name = canalEntrada.readLine();
       
            while (existeUser(users, name)) {
                canalSalida.println("El nombre de usuario ya se encuentre registrado\r\n");
                canalSalida.println("Ingrese un nuevo nombre\r\n");
                name = canalEntrada.readLine();
            }
            users.add(name);
            System.out.println(this.client.getRemoteSocketAddress());
           
            menuPrincipal(name);
            
            client.close();           
        }catch (Exception e){

        }    
    }
}
