# sdypp2022
**TP1**

- Ahora para ejecutar los ejercicios se pueden bajar directamente de Dockerhub:
- `sudo docker pull williamsoft/punto1y2`
- `sudo docker pull williamsoft/punto5-servidor`
- `sudo docker pull williamsoft/punto5-cliente`

NOTA: para saber en que direccion se levanta el contenedor servidor ejecutar: 
- `docker inspect [id del contenedor]`

NOTA2: para saber el id del contenedor ejecutar:
- `docker ps -a`

***Ejecutar servidor***
- Ejemplo: `sudo docker run -it williamsoft/punto4-servidor`

***Ejecutar cliente***
- `telnet 172.17.0.2 9090`
- Para visualizar los logs ejecutar:
    - docker logs [ID_DEL_CONTENEDOR]
    
Nota: El ID del contenedor se puede obtener haciendo docker ps -aq

**TP2**

Estamos trabajando en un repositorio de Fede, cuando terminemos planeamos hacer un fork.
